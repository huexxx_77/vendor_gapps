#
# Copyright (C) 2019-2021 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_DEVICE := $(lastword $(subst _, ,$(TARGET_PRODUCT)))

## Include Overlays
#PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += \
#    vendor/gapps/overlay \
#    vendor/gapps/overlay-pixel

#DEVICE_PACKAGE_OVERLAYS += \
#    vendor/gapps/overlay/common \
#    vendor/gapps/overlay-pixel/common

# RRO Overlays
PRODUCT_PACKAGES += \
    BackupTransportOverlay \
    ContactsMetadataOverlay \
    GoogleWebViewOverlay \
    TheGapps \
    TheGapps-Standard \
    TheGapps-Basics

# OPA configuration
PRODUCT_PRODUCT_PROPERTIES += \
    ro.opa.eligible_device=true

$(call inherit-product, vendor/gapps/common/common-vendor.mk)
